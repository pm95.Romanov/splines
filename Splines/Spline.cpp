#include "Spline.h"



namespace Splines
{
    int compare0(const double& x, const double& eps)
    {
        return (x > 0 ? (x > +eps ? +1 : 0) : (x < -eps ? -1 : 0));
    }

    int compare(const double& x, const double& y, const double& eps)
    {
        static thread_local double dif, a, b;
        dif = x - y;

        a = x > 0 ? x : -x;
        b = y > 0 ? y : -y;

        if (a > b)
        {
            if (dif > 0)
            {
                if (dif <= eps)
                {
                    return 0;
                }
                else
                {
                    if (dif <= eps * x)
                    {
                        return 0;
                    }
                    else
                    {
                        return +1;
                    }
                }
            }
            else
            {
                if (dif >= -eps)
                {
                    return 0;
                }
                else
                {
                    if (dif >= eps * x)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
        }
        else
        {
            if (dif > 0)
            {
                if (dif <= eps)
                {
                    return 0;
                }
                else
                {
                    if (dif <= -eps * y)
                    {
                        return 0;
                    }
                    else
                    {
                        return +1;
                    }
                }
            }
            else
            {
                if (dif >= -eps)
                {
                    return 0;
                }
                else
                {
                    if (dif >= -eps * y)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
        }
    }



    bool Splines::Spline::findSegment(const double& x, std::size_t& beginIndex, std::size_t& endIndex) const
    {
        bool successfully = false;
        auto begin = std::begin(m_points),
            end = std::end(m_points),
            middle = begin + (end - begin) / 2;

        while (middle != begin)
        {
            int cmp = compare(x, *middle, 1e-15);
            if (cmp == 0)
            {
                beginIndex = endIndex = middle - std::begin(m_points);
                successfully = true;
                break;
            }
            else if (cmp < 0)
            {
                end = middle;
            }
            else
            {
                begin = middle;
            }
            middle = begin + (end - begin) / 2;
        }

        if (!successfully)
        {
            int cmp = compare(x, *begin, 1e-15);
            if (cmp == 0)
            {
                beginIndex = endIndex = begin - std::begin(m_points);
                successfully = true;
            }
            else if (cmp < 0)
            {
                beginIndex = endIndex = -1;
            }
            else
            {
                cmp = compare(x, *end, 1e-15);
                if (cmp == 0)
                {
                    beginIndex = endIndex = end - std::begin(m_points);
                    successfully = true;
                }
                else if (cmp < 0)
                {
                    beginIndex = begin - std::begin(m_points);
                    endIndex = end - std::begin(m_points);
                }
                else
                {
                    beginIndex = endIndex = -1;
                }
            }
        }

        return successfully;
    }

    double Splines::Spline::getValue(const double& x) const
    {
        double value = 0;
        size_t beginSegment, endSegment;
        findSegment(x, beginSegment, endSegment);
        if (beginSegment == endSegment)
        {
            if (beginSegment > 0)
            {
                value = getValue(beginSegment - 1, 1);
            }
            else
            {
                value = getValue(beginSegment, -1);
            }
        }
        else
        {
            const std::valarray<double>& points = getPoints();
            double center = (points[endSegment] + points[beginSegment]) / 2.0,
                   halfLength = (points[endSegment] - points[beginSegment]) / 2.0;
            value = getValue(beginSegment, (x - center) / halfLength);
        }

        return value;
    }
}
