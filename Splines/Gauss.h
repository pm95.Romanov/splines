#pragma once
#include <array>



namespace Gauss
{
	constexpr std::array<double, 1> GaussPoints1d_1
	{
		+0.000000000000000
	};

	constexpr std::array<double, 2> GaussPoints1d_2
	{
		-0.577350269189626,
		+0.577350269189626,
	};

	constexpr std::array<double, 3> GaussPoints1d_3
	{
		-0.774596669241483,
		+0.000000000000000,
		+0.774596669241483,
	};

	constexpr std::array<double, 4> GaussPoints1d_4
	{
		-0.861136311594053,
		-0.339981043584856,
		+0.339981043584856,
		+0.861136311594053,
	};

	constexpr std::array<double, 5> GaussPoints1d_5
	{
		-0.90617984593866399280,
		-0.53846931010568309104,
		+0.00000000000000000000,
		+0.53846931010568309104,
		+0.90617984593866399280,
	};

	constexpr std::array<double, 6> GaussPoints1d_6
	{
		-0.932469514203152,
		-0.661209386466265,
		-0.238619186083197,
		+0.238619186083197,
		+0.661209386466265,
		+0.932469514203152,
	};

	constexpr std::array<double, 7> GaussPoints1d_7
	{
		-0.949107912342759,
		-0.741531185599394,
		-0.405845151377397,
		+0.000000000000000,
		+0.405845151377397,
		+0.741531185599394,
		+0.949107912342759,
	};

	constexpr std::array<double, 8> GaussPoints1d_8
	{
		-0.960289856497536,
		-0.796666477413627,
		-0.525532409916329,
		-0.183434642495650,
		+0.183434642495650,
		+0.525532409916329,
		+0.796666477413627,
		+0.960289856497536,
	};

	constexpr std::array<double, 9> GaussPoints1d_9
	{
		-0.968160239507626,
		-0.836031107326636,
		-0.613371432700590,
		-0.324253423403809,
		+0.000000000000000,
		+0.324253423403809,
		+0.613371432700590,
		+0.836031107326636,
		+0.968160239507626,
	};

	constexpr std::array<double, 10> GaussPoints1d_10
	{
		-0.973906528517172,
		-0.865063366688985,
		-0.679409568299024,
		-0.433395394129247,
		-0.148874338981631,
		+0.148874338981631,
		+0.433395394129247,
		+0.679409568299024,
		+0.865063366688985,
		+0.973906528517172,
	};


	constexpr std::array<double, 1> GaussWeights1d_1
	{
		2.0
	};

	constexpr std::array<double, 2> GaussWeights1d_2
	{
		1.0,
		1.0,
	};

	constexpr std::array<double, 3> GaussWeights1d_3
	{
		0.55555555555555555556,
		0.88888888888888888889,
		0.55555555555555555556,
	};

	constexpr std::array<double, 4> GaussWeights1d_4
	{
		0.347854845137454,
		0.652145154862546,
		0.652145154862546,
		0.347854845137454,
	};

	constexpr std::array<double, 5> GaussWeights1d_5
	{
		0.23692688505618908751,
		0.47862867049936646804,
		0.56888888888888888889,
		0.47862867049936646804,
		0.23692688505618908751,
	};

	constexpr std::array<double, 6> GaussWeights1d_6
	{
		0.171324492379170,
		0.360761573048139,
		0.467913934572691,
		0.467913934572691,
		0.360761573048139,
		0.171324492379170,
	};

	constexpr std::array<double, 7> GaussWeights1d_7
	{
		0.129484966168870,
		0.279705391489277,
		0.381830050505119,
		0.417959183673469,
		0.381830050505119,
		0.279705391489277,
		0.129484966168870,
	};

	constexpr std::array<double, 8> GaussWeights1d_8
	{
		0.101228536290376,
		0.222381034453374,
		0.313706645877887,
		0.362683783378362,
		0.362683783378362,
		0.313706645877887,
		0.222381034453374,
		0.101228536290376,
	};

	constexpr std::array<double, 9> GaussWeights1d_9
	{
		0.081274388361574,
		0.180648160694857,
		0.260610696402935,
		0.312347077040003,
		0.330239355001260,
		0.312347077040003,
		0.260610696402935,
		0.180648160694857,
		0.081274388361574,
	};

	constexpr std::array<double, 10> GaussWeights1d_10
	{
		0.066671344308688,
		0.149451349150581,
		0.219086362515982,
		0.269266719309996,
		0.295524224714753,
		0.295524224714753,
		0.269266719309996,
		0.219086362515982,
		0.149451349150581,
		0.066671344308688,
	};



	template <typename>
	struct closure_traits;

	template <typename FunctionT>
	struct closure_traits
		: closure_traits<decltype(&std::remove_reference_t<FunctionT>::operator())>
	{
	};

	template <typename ReturnTypeT, typename... Args>
	struct closure_traits<ReturnTypeT(Args...)>
	{
		using Arguments = std::tuple<Args...>;

		static constexpr std::size_t Arity = std::tuple_size<Arguments>::value;

		template <std::size_t N>
		using Argument = typename std::tuple_element<N, Arguments>;

		using FirstArgument = typename std::tuple_element<0, Arguments>;
		using SecondArgument = typename std::tuple_element<1, Arguments>;
		using ThirdArgument = typename std::tuple_element<2, Arguments>;
		using FourthArgument = typename std::tuple_element<3, Arguments>;
		using FivethArgument = typename std::tuple_element<4, Arguments>;
		using SixthArgument = typename std::tuple_element<5, Arguments>;

		using ReturnType = ReturnTypeT;
	};

	template <typename ReturnTypeT, typename... Args>
	struct closure_traits<ReturnTypeT(*)(Args...)>
		: closure_traits<ReturnTypeT(Args...)>
	{
	};

	template <typename ReturnTypeT, typename... Args>
	struct closure_traits<ReturnTypeT(&)(Args...)>
		: closure_traits<ReturnTypeT(Args...)>
	{
	};

	template <typename ReturnTypeT, typename... Args>
	struct closure_traits<ReturnTypeT(&&)(Args...)>
		: closure_traits<ReturnTypeT(Args...)>
	{
	};

	template <typename ReturnTypeT, typename ClassTypeT, typename... Args>
	struct closure_traits<ReturnTypeT(ClassTypeT::*)(Args...)>
		: closure_traits<ReturnTypeT(Args...)>
	{
		using class_type = ClassTypeT;
	};

	template <typename ReturnTypeT, typename ClassTypeT, typename... Args>
	struct closure_traits<ReturnTypeT(ClassTypeT::*)(Args...) const>
		: closure_traits<ReturnTypeT(ClassTypeT::*)(Args...)>
	{
	};



	template<int L, int D, int K = 0, typename T = double>
	class Gauss
	{
		static_assert(L > 0 && L < 11, "Amount of points must be large than 0 and less than 11.");
		static_assert(D > 0 && D < 4, "The dimension must be large than 0 and less than 4.");
		static_assert(K >= 0, "Amount of prepared values must be large than 0.");
	
	private:
		/*WolframAlpha "Solve LegendreP(L, x) = 0"*/
		static constexpr std::array<double, L> getPoints1d()
		{
			if constexpr (L == 1)
			{
				return GaussPoints1d_1;
			}
			else if constexpr (L == 2)
			{
				return GaussPoints1d_2;
			}
			else if constexpr (L == 3)
			{
				return GaussPoints1d_3;
			}
			else if constexpr (L == 4)
			{
				return GaussPoints1d_4;
			}
			else if constexpr (L == 5)
			{
				return GaussPoints1d_5;
			}
			else if constexpr (L == 6)
			{
				return GaussPoints1d_6;
			}
			else if constexpr (L == 7)
			{
				return GaussPoints1d_7;
			}
			else if constexpr (L == 8)
			{
				return GaussPoints1d_8;
			}
			else if constexpr (L == 9)
			{
				return GaussPoints1d_9;
			}
			else if constexpr (L == 10)
			{
				return GaussPoints1d_10;
			}
			else
			{
				throw new std::exception();
			}
		}
	
		/*WolframAlpha "w = 2/((1-x^2) * (d(LegendreP(L, x))/dx)^2), LegendreP(L, x) = 0"*/
		static constexpr std::array<double, L> getWeights1d()
		{
			if constexpr (L == 1)
			{
				return GaussWeights1d_1;
			}
			else if constexpr (L == 2)
			{
				return GaussWeights1d_2;
			}
			else if constexpr (L == 3)
			{
				return GaussWeights1d_3;
			}
			else if constexpr (L == 4)
			{
				return GaussWeights1d_4;
			}
			else if constexpr (L == 5)
			{
				return GaussWeights1d_5;
			}
			else if constexpr (L == 6)
			{
				return GaussWeights1d_6;
			}
			else if constexpr (L == 7)
			{
				return GaussWeights1d_7;
			}
			else if constexpr (L == 8)
			{
				return GaussWeights1d_8;
			}
			else if constexpr (L == 9)
			{
				return GaussWeights1d_9;
			}
			else if constexpr (L == 10)
			{
				return GaussWeights1d_10;
			}
			else
			{
				throw new std::exception();
			}
		};


		std::valarray<std::valarray<T>> m_preperedValues;
	
	public:
		static constexpr std::array<double, L> Points1d = getPoints1d();
		static constexpr std::array<double, L> Weights1d = getWeights1d();
	
	
		Gauss()
		{
			if constexpr (D == 1)
			{
				m_preperedValues = std::valarray<std::valarray<T>>(std::valarray<T>(K), L);
			}
			else if constexpr (D == 2)
			{
				m_preperedValues = std::valarray<std::valarray<T>>(std::valarray<T>(K), L * L);
			}
			else if constexpr (D == 3)
			{
				m_preperedValues = std::valarray<std::valarray<T>>(std::valarray<T>(K), L * L * L);
			}
		}
	
	
		template <class TF>
		static constexpr double integrate(TF&& f)
		{
			using traits = closure_traits<TF>;
		
			static_assert(std::is_convertible<traits::ReturnType, T>::value,
						  "Invalid function");
		
			double value = 0;
		
			if constexpr (D == 1)
			{
				static_assert(D == 1 && traits::Arity == 1 && std::is_convertible<double, traits::FirstArgument::type>::value,
							  "Invalid function");
				
				for (int i = 0; i < L; i++)
				{
					value += Weights1d[i] * f(Points1d[i]);
				}
			}
			else if constexpr (D == 2)
			{
				static_assert(D == 2 && traits::Arity == 2 && std::is_convertible<double, traits::FirstArgument::type>::value &&
															  std::is_convertible<double, traits::SecondArgument::type>::value,
							  "Invalid function");
				
				for (int j = 0; j < L; j++)
				{
					for (int i = 0; i < L; i++)
					{
						value += Weights1d[i] * Weights1d[j] * f(Points1d[i], Points1d[j]);
					}
				}
			}
			else if constexpr (D == 3)
			{
				static_assert(D == 3 && traits::Arity == 3 && std::is_convertible<double, traits::FirstArgument::type>::value &&
															  std::is_convertible<double, traits::SecondArgument::type>::value &&
															  std::is_convertible<double, traits::ThirdArgument::type>::value,
							  "Invalid function");
				
				for (int k = 0; k < L; k++)
				{
					for (int j = 0; j < L; j++)
					{
						for (int i = 0; i < L; i++)
						{
							value += Weights1d[i] * Weights1d[j] * Weights1d[k] * f(Points1d[i], Points1d[j], Points1d[k]);
						}
					}
				}
			}
		
			return value;
		}
		
		template <int N, class TF>
		static constexpr double integrate(TF&& f)
		{
			using traits = closure_traits<TF>;

			static_assert(std::is_convertible<traits::ReturnType, T>::value,
						  "Invalid function");
		
			static thread_local std::array<double, N> values;
			values.fill(0);
		
			if constexpr (D == 1)
			{
				static_assert(D == 1 && traits::Arity == 1 && std::is_convertible<double, traits::FirstArgument::type>::value,
							  "Invalid function");
				
				for (int i = 0; i < L; i++)
				{
					for (int n = 0; n < N; n++)
					{
						values[n] += Weights1d[i] * f(Points1d[i]);
					}
				}
			}
			else if constexpr (D == 2)
			{
				static_assert(D == 2 && traits::Arity == 2 && std::is_convertible<double, traits::FirstArgument::type>::value &&
															  std::is_convertible<double, traits::SecondArgument::type>::value,
							  "Invalid function");
				
				for (int j = 0; j < L; j++)
				{
					for (int i = 0; i < L; i++)
					{
						for (int n = 0; n < N; n++)
						{
							values[n] += Weights1d[i] * Weights1d[j] * f(Points1d[i], Points1d[j]);
						}
					}
				}
			}
			else if constexpr (D == 3)
			{
				static_assert(D == 3 && traits::Arity == 3 && std::is_convertible<double, traits::FirstArgument::type>::value &&
															  std::is_convertible<double, traits::SecondArgument::type>::value &&
															  std::is_convertible<double, traits::ThirdArgument::type>::value,
							  "Invalid function");
				
				for (int k = 0; k < L; k++)
				{
					for (int j = 0; j < L; j++)
					{
						for (int i = 0; i < L; i++)
						{
							for (int n = 0; n < N; n++)
							{
								values[n] += Weights1d[i] * Weights1d[j] * Weights1d[k] * f(Points1d[i], Points1d[j], Points1d[k]);
							}
						}
					}
				}
			}
		
			return values;
		}
		
		template <int N, int M, class TF>
		static constexpr double integrate(TF&& f)
		{
			using traits = closure_traits<TF>;

			static_assert(std::is_convertible<traits::ReturnType, T>::value,
						  "Invalid function");
		
			static thread_local std::array<std::array<double, M>, N> values;
			values.fill(0);
		
			if constexpr (D == 1)
			{
				static_assert(D == 1 && traits::Arity == 1 && std::is_convertible<double, traits::FirstArgument::type>::value,
							  "Invalid function");
				
				for (int i = 0; i < L; i++)
				{
					for (int n = 0; n < N; n++)
					{
						for (int m = 0; m < M; m++)
						{
							values[n][m] += Weights1d[i] * f(Points1d[i]);
						}
					}
				}
			}
			else if constexpr (D == 2)
			{
				static_assert(D == 2 && traits::Arity == 2 && std::is_convertible<double, traits::FirstArgument::type>::value &&
															  std::is_convertible<double, traits::SecondArgument::type>::value,
							  "Invalid function");
				
				for (int j = 0; j < L; j++)
				{
					for (int i = 0; i < L; i++)
					{
						for (int n = 0; n < N; n++)
						{
							for (int m = 0; m < M; m++)
							{
								values[n][m] += Weights1d[i] * Weights1d[j] * f(Points1d[i], Points1d[j]);
							}
						}
					}
				}
			}
			else if constexpr (D == 3)
			{
				static_assert(D == 3 && traits::Arity == 3 && std::is_convertible<double, traits::FirstArgument::type>::value &&
															  std::is_convertible<double, traits::SecondArgument::type>::value &&
															  std::is_convertible<double, traits::ThirdArgument::type>::value,
							  "Invalid function");
				
				for (int k = 0; k < L; k++)
				{
					for (int j = 0; j < L; j++)
					{
						for (int i = 0; i < L; i++)
						{
							for (int n = 0; n < N; n++)
							{
								for (int m = 0; m < M; m++)
								{
									values[n][m] += Weights1d[i] * Weights1d[j] * Weights1d[k] * f(Points1d[i], Points1d[j], Points1d[k]);
								}
							}
						}
					}
				}
			}
		
			return values;
		}
	};
}