#include "InterpolationQuadraticSpline.h"
#include "InterpolationCubicSpline.h"
#include "InterpolationCubicHermiteSpline.h"
#include "SmoothingCubicHermitSpline.h"
#include "Gauss.h"

#include <cstdio>
#include <locale.h>
#include <memory>



inline static double f1(const double& x)
{
    return x;
}

inline static double f2(const double& x)
{
    return x * x;
}

inline static double f3(const double& x)
{
    return x > 0 ? x + x * x / 2 - x * x * x / 90 :
                   x + x * x / 2 + x * x * x / 90;
}

inline static double f4(const double& x)
{
    return sin(x);
}

const std::function<double(const double& x)> f = f4;



int main()
{
    constexpr std::size_t numberOfSegments = 20;
    constexpr double start = -15.0, end = 15.0, step = (end - start) / numberOfSegments;
    std::valarray<double> points(numberOfSegments + 1);
    std::valarray<double> values(numberOfSegments + 1);
    std::valarray<double> weights(numberOfSegments + 1);
    for (std::size_t i = 0; i <= numberOfSegments; i++)
    {
        points[i] = start + i * step;
        values[i] = f(points[i]);
        weights[i] = 1.0;
    }

    constexpr std::size_t numberOfSegmentsForSmoothingSpline = 7;
    constexpr double startForSmoothingSpline = -15.0,
                     endForSmoothingSpline   = +15.0,
                     stepForSmoothingSpline = (endForSmoothingSpline - startForSmoothingSpline) / numberOfSegmentsForSmoothingSpline;
    std::valarray<double> pointsForSmoothingSpline(numberOfSegmentsForSmoothingSpline + 1);
    std::valarray<double> alpha(numberOfSegmentsForSmoothingSpline + 1);
    std::valarray<double> beta(numberOfSegmentsForSmoothingSpline + 1);
    for (std::size_t i = 0; i <= numberOfSegmentsForSmoothingSpline; i++)
    {
        pointsForSmoothingSpline[i] = startForSmoothingSpline + i * stepForSmoothingSpline;
        alpha[i] = 0.0;
        beta[i] = 0.0;
    }

    std::array<std::unique_ptr<Splines::Spline>, 5> splines
    {
        std::make_unique<Splines::InterpolationQuadraticSpline>(points, values),
        std::make_unique<Splines::InterpolationCubicSpline>(points, values),
        std::make_unique<Splines::InterpolationCubicHermiteSpline>(points, values, Splines::JoiningFragmentsType::APPROXE_FIRST_DERIVATIVE),
        std::make_unique<Splines::InterpolationCubicHermiteSpline>(points, values, Splines::JoiningFragmentsType::SMOOTHNESS),
        std::make_unique<Splines::SmoothingCubicHermitSpline>(points, values, weights, pointsForSmoothingSpline, alpha, beta),
    };

    std::array<double, splines.size()> normDiff;
    std::fill(normDiff.begin(), normDiff.end(), 0);

    for (std::size_t l = 0; l < splines.size(); l++)
    {
        std::size_t splineNumberOfSegment = splines[l]->getPoints().size() - 1;
        for (std::size_t i = 0; i < splineNumberOfSegment; i++)
        {
            normDiff[l] += splines[l]->calculateSquareError(i, f);
        }
    }

    double normF = 0;
    for (std::size_t i = 0; i < numberOfSegments; i++)
    {
        double center = (points[i + 1] + points[i]) / 2.0,
               halfLength = (points[i + 1] - points[i]) / 2.0;

        normF += halfLength * Gauss::Gauss<5, 1>::integrate([&](const double& xi)
        {
            double buf = f(center + halfLength * xi);
            return buf * buf;
        });
    }


    FILE* file;
    if (fopen_s(&file, "Splines.csv", "w") == 0)
    {
        auto locale = setlocale(LC_ALL, "Russian");

        try
        {
            constexpr int numberOfInnerPoints = 25;
            constexpr double localH = 2.0 / (numberOfInnerPoints + 1);
            constexpr size_t numberOfPoints = numberOfSegments * (1 + numberOfInnerPoints) + 1;

            fprintf_s(file, "\"||S - f||_0 / ||f||_0\";\"\";\"\";");
            for (size_t segmentIndex = 0, k = 0; segmentIndex < numberOfSegments; segmentIndex++)
            {
                fprintf_s(file, "\"%.15e\";", points[segmentIndex]);

                double h = (points[segmentIndex + 1] - points[segmentIndex]) / (numberOfInnerPoints + 2);
                for (int pointIndex = 1; pointIndex <= numberOfInnerPoints; pointIndex++, k++)
                {
                    double x = points[segmentIndex] + pointIndex * h, xi = -1 + pointIndex * localH;
                    fprintf_s(file, "\"%.15e\";", x);
                }
            }
            fprintf_s(file, "\"%.15e\"\n", points[numberOfSegments]);

            fprintf_s(file, "\"\";\"\";\"f\";");
            for (size_t segmentIndex = 0, k = 0; segmentIndex < numberOfSegments; segmentIndex++)
            {
                fprintf_s(file, "\"%.15e\";", f(points[segmentIndex]));

                double h = (points[segmentIndex + 1] - points[segmentIndex]) / (numberOfInnerPoints + 2);
                for (int pointIndex = 1; pointIndex <= numberOfInnerPoints; pointIndex++, k++)
                {
                    double x = points[segmentIndex] + pointIndex * h, xi = -1 + pointIndex * localH;
                    fprintf_s(file, "\"%.15e\";", f(x));
                }
            }
            fprintf_s(file, "\"%.15e\"\n", f(points[numberOfSegments]));

            for (int l = 0; l < splines.size(); l++)
            {
                fprintf_s(file, "\"%.15e\";\"\";\"S_%d\";", sqrt(normDiff[l] / normF), l);
                for (size_t segmentIndex = 0, k = 0; segmentIndex < numberOfSegments; segmentIndex++)
                {
                    fprintf_s(file, "\"%.15e\";", splines[l]->getValue(points[segmentIndex]));

                    double h = (points[segmentIndex + 1] - points[segmentIndex]) / (numberOfInnerPoints + 2);
                    for (int pointIndex = 1; pointIndex <= numberOfInnerPoints; pointIndex++, k++)
                    {
                        double x = points[segmentIndex] + pointIndex * h, xi = -1 + pointIndex * localH;
                        fprintf_s(file, "\"%.15e\";", splines[l]->getValue(x));
                    }
                }
                fprintf_s(file, "\"%.15e\"\n", splines[l]->getValue(points[numberOfSegments]));
            }
        }
        catch (...)
        {

        }

        fclose(file);

        setlocale(LC_ALL, locale);
    }

    for (int l = 0; l < splines.size(); l++)
    {
        printf_s("||S_%d - f||_0 / ||f||_0 = %.15e\n", l, sqrt(normDiff[l] / normF));
    }

    return 0;
}