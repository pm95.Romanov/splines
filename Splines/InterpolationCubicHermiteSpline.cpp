#include "InterpolationCubicHermiteSpline.h"

#include "Gauss.h"



namespace Splines
{
    std::valarray<double> Splines::InterpolationCubicHermiteSpline::interpolate(const std::valarray<double>& points,
                                                                                const std::valarray<double>& values,
                                                                                const Splines::JoiningFragmentsType& joiningFragmentsType)
    {
        if (points.size() != values.size())
        {
            throw std::invalid_argument("Points.size() != values.size()");
        }

        std::valarray<double> weights;
        switch (joiningFragmentsType)
        {
            case Splines::JoiningFragmentsType::APPROXE_FIRST_DERIVATIVE:
            {
                weights = interpolateWithDerivativeApproximation(points, values);
                break;
            }

            case Splines::JoiningFragmentsType::SMOOTHNESS:
            {
                weights = interpolateWithSmoothness(points, values);
                break;
            }

            default:
                throw Splines::NotImplementedException();
        }

        return weights;
    }

    std::valarray<double> Splines::InterpolationCubicHermiteSpline::interpolateWithDerivativeApproximation(const std::valarray<double>& points,
                                                                                                           const std::valarray<double>& values)
    {
        std::size_t numberOfPoints = points.size(),
                    numberOfSegments = numberOfPoints - 1;
        if (numberOfPoints < 3)
        {
            throw std::invalid_argument("Points.size() < 3");
        }

        std::valarray<double> weights(2 * numberOfPoints);
        double prevH = points[1] - points[0],
               currentH = points[2] - points[1],
               sumH = prevH + currentH;
        weights[0] = values[0];
        weights[1] = -(prevH + sumH) / (prevH * sumH) * values[0] +
                      sumH / (prevH * currentH) * values[1] -
                      prevH / (sumH * currentH) * values[2];
        
        weights[2] = values[1];
        weights[3] = -currentH / (prevH * sumH) * values[0] +
                      (currentH - prevH) / (prevH * currentH) * values[1] +
                      prevH / (sumH * currentH) * values[2];
        for (std::size_t i = 2; i < numberOfSegments; i++)
        {
            prevH = currentH;
            currentH = points[i + 1] - points[i];
            sumH = prevH + currentH;

            weights[2 * i] = values[i];
            weights[2 * i + 1] = -currentH / (prevH * sumH) * values[i - 1] +
                                  (currentH - prevH) / (prevH * currentH) * values[i] +
                                  prevH / (sumH * currentH) * values[i + 1];
        }
    
        weights[2 * numberOfSegments] = values[numberOfSegments];
        weights[2 * numberOfSegments + 1] = currentH / (prevH * sumH) * values[numberOfSegments - 2] -
                                            sumH / (prevH * currentH) * values[numberOfSegments - 1] +
                                            (sumH + currentH) / (sumH * currentH) * values[numberOfSegments];

        return weights;
    }

    std::valarray<double> Splines::InterpolationCubicHermiteSpline::interpolateWithSmoothness(const std::valarray<double>& points,
                                                                                              const std::valarray<double>& values)
    {
        std::size_t numberOfPoints = points.size(),
                    numberOfSegments = numberOfPoints - 1;
        if (numberOfPoints < 3)
        {
            throw std::invalid_argument("Points.size() < 3");
        }

        std::valarray<double> weights(2 * numberOfPoints);
        std::valarray<double> A(2 * numberOfSegments);
        double prevH = points[1] - points[0],
               currentH = points[2] - points[1],
               sumH = prevH + currentH;
        A[0] = 1;
        A[1] = 0;
        weights[0] = values[0];
        weights[1] = -(prevH + sumH) / (prevH * sumH) * values[0] +
                      sumH / (prevH * currentH) * values[1] -
                      prevH / (sumH * currentH) * values[2];

        A[2] = 4.0 * (1.0 / prevH + 1.0 / currentH);
        A[3] = 2.0 / currentH;
        weights[2] = values[1];
        weights[3] = 6.0 * (-values[0] / (prevH * prevH) +
                             values[1] * (1.0 / (prevH * prevH) - 1.0 / (currentH * currentH)) +
                             values[2] / (currentH * currentH)) - 2.0 / prevH * weights[1];

        for (std::size_t row = 2; row < numberOfSegments; row++)
        {
            prevH = currentH;
            currentH = points[row + 1] - points[row];
            double a = 2.0 / prevH,
                   k = a / A[2 * (row - 1)];

            A[2 * row] = 4.0 * (1.0 / prevH + 1.0 / currentH) - k * A[2 * row - 1];
            A[2 * row + 1] = 2.0 / currentH;
            weights[2 * row] = values[row];
            weights[2 * row + 1] = 6.0 * (-values[row - 1] / (prevH * prevH) +
                                           values[row] * (1.0 / (prevH * prevH) - 1.0 / (currentH * currentH)) +
                                           values[row + 1] / (currentH * currentH)) - k * weights[2 * row - 1];
        }

        sumH = prevH + currentH;
        weights[2 * numberOfSegments] = values[numberOfSegments];
        weights[2 * numberOfSegments + 1] = currentH / (prevH * sumH) * values[numberOfSegments - 2] -
                                            sumH / (prevH * currentH) * values[numberOfSegments - 1] +
                                            (sumH + currentH) / (sumH * currentH) * values[numberOfSegments];

        for (std::size_t row = numberOfSegments - 1; row > 0; row--)
        {
            weights[2 * row + 1] = (weights[2 * row + 1] - A[2 * row + 1] * weights[2 * row + 3]) / A[2 * row];
        }

        return weights;
    }



    Splines::InterpolationCubicHermiteSpline::InterpolationCubicHermiteSpline(const std::valarray<double>& points,
                                                                              const std::valarray<double>& values,
                                                                              const Splines::JoiningFragmentsType& joiningFragmentsType) :
        Splines::CubicHermitSpline(points, interpolate(points, values, joiningFragmentsType)), JoiningFragmentsType(joiningFragmentsType)
    {

    }
}
