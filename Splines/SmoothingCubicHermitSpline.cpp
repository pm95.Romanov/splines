#include "SmoothingCubicHermitSpline.h"

#include "Gauss.h"



namespace Splines
{
    std::valarray<double> Splines::SmoothingCubicHermitSpline::interpolate(const std::valarray<double>& points,
                                                                           const std::valarray<double>& values,
                                                                           const std::valarray<double>& weights,
                                                                           const std::valarray<double>& segmentPoints,
                                                                           const std::valarray<double>& alpha,
                                                                           const std::valarray<double>& beta)
    {
        std::size_t numberOfSegments = segmentPoints.size() - 1;
        if (numberOfSegments < 2)
        {
            throw std::invalid_argument("Points.size() < 3");
        }
        std::valarray<double> w(0.0, 2 * (numberOfSegments + 1));
        std::valarray<double> A(0.0, 7 * (numberOfSegments + 1));

        // X X | X x x x 0 0 0 0 0 0 |          | w_0 |    | b_0 |
        // X X | X X x x 0 0 0 0 0 0 |          | w_1 |    | b_1 |
        //     | X X X x x x 0 0 0 0 |          | w_2 |    | b_2 |
        //     | X X X X x x 0 0 0 0 |          | w_3 |    | b_3 |
        //     | 0 0 X X X x x x 0 0 |      \/  | w_4 | -- | b_4 |
        //     | 0 0 X X X X x x 0 0 |      /\  | w_5 | -- | b_5 |
        //     | 0 0 0 0 X X X x x x |          | w_6 |    | b_6 |
        //     | 0 0 0 0 X X X X x x |          | w_7 |    | b_7 |
        //     | 0 0 0 0 0 0 X X X x | x x      | w_8 |    | b_8 |
        //     | 0 0 0 0 0 0 X X X X | x x      | w_9 |    | b_9 |


        //������ �������
        std::array<std::array<double, 4>, 4> M;
        std::array<double, 4> psiValues, b;
        std::size_t pointIndex = 0, numberOfPoints = points.size();
        double h, xi, center, length, halfLength;
        double *ptrA = std::begin(A), *ptrW = std::begin(w);
        while (pointIndex < numberOfPoints && points[pointIndex] < segmentPoints[0])
        {
            pointIndex++;
        }

        for (std::size_t segmentIndex = 0; pointIndex < numberOfPoints && segmentIndex < numberOfSegments - 1; segmentIndex++, ptrA += 7, ptrW += 2)
        {
            h = segmentPoints[segmentIndex + 1] - segmentPoints[segmentIndex];

            //��������� ������
            //alpha + beta
            M[0][0] = alpha[segmentIndex] * 6.0 / (5.0 * h) + beta[segmentIndex] * 60.0 / (h * h * h);
            M[0][1] = alpha[segmentIndex] / 10.0 + beta[segmentIndex] * 30.0 / (h * h);
            M[0][2] = -M[0][0];
            M[0][3] = M[0][1];

            M[1][0] = M[0][1];
            M[1][1] = alpha[segmentIndex] * h * 2.0 / 15.0 + beta[segmentIndex] * 16.0 / h;
            M[1][2] = -M[0][1];
            M[1][3] = -alpha[segmentIndex] * h / 30.0 + beta[segmentIndex] * 14 / h;

            M[2][0] = M[0][2];
            M[2][1] = M[1][2];
            M[2][2] = M[0][0];
            M[2][3] = M[1][2];

            M[3][0] = M[1][0];
            M[3][1] = M[1][3];
            M[3][2] = M[1][2];
            M[3][3] = M[1][1];

            //A
            center = (segmentPoints[segmentIndex + 1] + segmentPoints[segmentIndex]) / 2.0;
            length = segmentPoints[segmentIndex + 1] - segmentPoints[segmentIndex];
            halfLength = length / 2.0;
            b[0] = b[1] = b[2] = b[3] = 0;
            while (pointIndex < numberOfPoints && points[pointIndex] < segmentPoints[segmentIndex + 1])
            {
                xi = (points[pointIndex] - center) / halfLength;
                psiValues[0] = baseFunction1(xi);
                psiValues[1] = length * baseFunction2(xi);
                psiValues[2] = baseFunction3(xi);
                psiValues[3] = length * baseFunction4(xi);

                for (std::size_t i = 0; i < psiValues.size(); i++)
                {
                    for (std::size_t j = 0; j <= i; j++)
                    {
                        M[i][j] += weights[pointIndex] * psiValues[i] * psiValues[j];
                    }
                    b[i] += weights[pointIndex] * psiValues[i] * values[pointIndex];
                }

                pointIndex++;
            }

            //�������� ��������� ������� � ���������
            // X  X  |  X  x  x  x  -  -  -  -  -  -  |    
            // X  X  |  X  X  x  x  -  -  -  -  -  -  |    
            //       | -7 -6 -5  x  x  x  -  -  -  -  |    
            //       | -4 -3 -2 -1  x  x  -  -  -  -  |    
            //       |  -  -  0  1  2  x  x  x  -  -  |    
            //       |  -  -  3  4  5  6  x  x  -  -  |    
            //       |  -  -  -  -  7  8  9  x  x  x  |    
            //       |  -  -  -  - 10 11 12 13  x  x  |    
            //       |  -  -  -  -  -  -  X  X  X  x  |  x  x
            //       |  -  -  -  -  -  -  X  X  X  X  |  x  x
            ptrA[2] += M[0][0];

            ptrA[5] += M[1][0];
            ptrA[6] += M[1][1];

            ptrA[7] += M[2][0];
            ptrA[8] += M[2][1];
            ptrA[9] += M[2][2];

            ptrA[10] += M[3][0];
            ptrA[11] += M[3][1];
            ptrA[12] += M[3][2];
            ptrA[13] += M[3][3];

            ptrW[0] += b[0];
            ptrW[1] += b[1];
            ptrW[2] += b[2];
            ptrW[3] += b[3];
        }

        h = segmentPoints[numberOfSegments] - segmentPoints[numberOfSegments - 1];

        //��������� ������
        //alpha + beta
        M[0][0] = alpha[numberOfSegments - 1] * 6.0 / (5.0 * h) + beta[numberOfSegments - 1] * 60.0 / (h * h * h);
        M[0][1] = alpha[numberOfSegments - 1] / 10.0 + beta[numberOfSegments - 1] * 30.0 / (h * h);
        M[0][2] = -M[0][0];
        M[0][3] = M[0][1];

        M[1][0] = M[0][1];
        M[1][1] = alpha[numberOfSegments - 1] * h * 2.0 / 15.0 + beta[numberOfSegments - 1] * 16.0 / h;
        M[1][2] = -M[0][1];
        M[1][3] = -alpha[numberOfSegments - 1] * h / 30.0 + beta[numberOfSegments - 1] * 14 / h;

        M[2][0] = M[0][2];
        M[2][1] = M[1][2];
        M[2][2] = M[0][0];
        M[2][3] = M[1][2];

        M[3][0] = M[1][0];
        M[3][1] = M[1][3];
        M[3][2] = M[1][2];
        M[3][3] = M[1][1];

        //A
        center = (segmentPoints[numberOfSegments] + segmentPoints[numberOfSegments - 1]) / 2.0;
        length = segmentPoints[numberOfSegments] - segmentPoints[numberOfSegments - 1];
        halfLength = length / 2.0;
        b[0] = b[1] = b[2] = b[3] = 0;
        while (pointIndex < numberOfPoints && points[pointIndex] <= segmentPoints[numberOfSegments])
        {
            xi = (points[pointIndex] - center) / halfLength;
            psiValues[0] = baseFunction1(xi);
            psiValues[1] = length * baseFunction2(xi);
            psiValues[2] = baseFunction3(xi);
            psiValues[3] = length * baseFunction4(xi);

            for (std::size_t i = 0; i < psiValues.size(); i++)
            {
                for (std::size_t j = 0; j <= i; j++)
                {
                    M[i][j] += weights[pointIndex] * psiValues[i] * psiValues[j];
                }
                b[i] += weights[pointIndex] * psiValues[i] * values[pointIndex];
            }

            pointIndex++;
        }

        //�������� ��������� ������� � ���������
        // X  X  |  X  x  x  x  -  -  -  -  -  -  |    
        // X  X  |  X  X  x  x  -  -  -  -  -  -  |    
        //       |  X  X  X  x  x  x  -  -  -  -  |    
        //       |  X  X  X  X  x  x  -  -  -  -  |    
        //       |  -  -  -7 -6 -5  x  x  x  -  - |    
        //       |  -  -  -4 -3 -2 -1  x  x  -  - |    
        //       |  -  -   -  -  0  1  2  x  x  x |    
        //       |  -  -   -  -  3  4  5  6  x  x |    
        //       |  -  -   -  -  -  -  7  8  9  x |  x  x
        //       |  -  -   -  -  -  - 10 11 12 13 |  x  x
        ptrA[2] += M[0][0];

        ptrA[5] += M[1][0];
        ptrA[6] += M[1][1];

        ptrA[7] += M[2][0];
        ptrA[8] += M[2][1];
        ptrA[9] += M[2][2];

        ptrA[10] += M[3][0];
        ptrA[11] += M[3][1];
        ptrA[12] += M[3][2];
        ptrA[13] += M[3][3];

        ptrW[0] += b[0];
        ptrW[1] += b[1];
        ptrW[2] += b[2];
        ptrW[3] += b[3];


        //LDLt ����������
        // X  X  |  X  x  x  x  -  -  -  -  -  -  |    
        // X  X  |  X  X  x  x  -  -  -  -  -  -  |    
        //       | -7 -6 -5  x  x  x  -  -  -  -  |    
        //       | -4 -3 -2 -1  x  x  -  -  -  -  |    
        //       |  -  -  0  1  2  x  x  x  -  -  |    
        //       |  -  -  3  4  5  6  x  x  -  -  |    
        //       |  -  -  -  -  7  8  9  x  x  x  |    
        //       |  -  -  -  - 10 11 12 13  x  x  |    
        //       |  -  -  -  -  -  -  X  X  X  x  | x x
        //       |  -  -  -  -  -  -  X  X  X  X  | x x
        A[5] = A[5] / A[2];
        A[6] = A[6] - A[5] * A[5] * A[2];
        ptrA = begin(A) + 7;
        for (std::size_t segmentPointIndex = 1; segmentPointIndex <= numberOfSegments; segmentPointIndex++, ptrA += 7)
        {
            ptrA[0] = ptrA[0] / ptrA[-5];
            ptrA[1] = (ptrA[1] - ptrA[0] * ptrA[-5] * ptrA[-2]) / ptrA[-1];
            ptrA[2] = (ptrA[2] - ptrA[0] * ptrA[0] * ptrA[-5] - ptrA[1] * ptrA[1] * ptrA[-1]);

            ptrA[3] = ptrA[3] / ptrA[-5];
            ptrA[4] = (ptrA[4] - ptrA[3] * ptrA[-5] * ptrA[-2]) / ptrA[-1];
            ptrA[5] = (ptrA[5] - ptrA[3] * ptrA[-5] * ptrA[0] - ptrA[4] * ptrA[-1] * ptrA[1]) / ptrA[2];
            ptrA[6] = (ptrA[6] - ptrA[3] * ptrA[3] * ptrA[-5] - ptrA[4] * ptrA[4] * ptrA[-1] - ptrA[5] * ptrA[5] * ptrA[2]);
        }

        //DL * y = b
        // X  X  |  X  x  x  x  -  -  -  -  -  -  |    
        // X  X  |  X  X  x  x  -  -  -  -  -  -  |    
        //       | -7 -6 -5  x  x  x  -  -  -  -  |    
        //       | -4 -3 -2 -1  x  x  -  -  -  -  |    
        //       |  -  -  0  1  2  x  x  x  -  -  |    
        //       |  -  -  3  4  5  6  x  x  -  -  |    
        //       |  -  -  -  -  7  8  9  x  x  x  |    
        //       |  -  -  -  - 10 11 12 13  x  x  |    
        //       |  -  -  -  -  -  -  X  X  X  x  |  x  x
        //       |  -  -  -  -  -  -  X  X  X  X  |  x  x
        w[1] -= w[0] * A[5];
        ptrA = begin(A) + 7;
        ptrW = std::begin(w) + 2;
        for (std::size_t segmentPointIndex = 1; segmentPointIndex <= numberOfSegments; segmentPointIndex++, ptrA += 7, ptrW += 2)
        {
            ptrW[0] -= ptrW[-2] * ptrA[0];
            ptrW[0] -= ptrW[-1] * ptrA[1];

            ptrW[1] -= ptrW[-2] * ptrA[3];
            ptrW[1] -= ptrW[-1] * ptrA[4];
            ptrW[1] -= ptrW[-0] * ptrA[5];

            ptrW[-2] /= ptrA[-5];
            ptrW[-1] /= ptrA[-1];
        }
        ptrW[-2] /= ptrA[-5];
        ptrW[-1] /= ptrA[-1];

        //Lt * w = y
        // X  X  |  X  x  x  x  -  -  -  -  -  -  |    
        // X  X  |  X  X  x  x  -  -  -  -  -  -  |    
        //       | -7 -6 -5  x  x  x  -  -  -  -  |    
        //       | -4 -3 -2 -1  x  x  -  -  -  -  |    
        //       |  -  -  0  1  2  x  x  x  -  -  |    
        //       |  -  -  3  4  5  6  x  x  -  -  |    
        //       |  -  -  -  -  7  8  9  x  x  x  |    
        //       |  -  -  -  - 10 11 12 13  x  x  |    
        //       |  -  -  -  -  -  -  X  X  X  x  |  x  x
        //       |  -  -  -  -  -  -  X  X  X  X  |  x  x
        ptrA -= 7;
        ptrW -= 2;
        for (std::size_t segmentPointIndex = numberOfSegments; segmentPointIndex > 0; segmentPointIndex--, ptrA -= 7, ptrW -= 2)
        {
            ptrW[+0] -= ptrW[1] * ptrA[5];
            ptrW[-1] -= ptrW[1] * ptrA[4];
            ptrW[-2] -= ptrW[1] * ptrA[3];
            
            ptrW[-1] -= ptrW[0] * ptrA[1];
            ptrW[-2] -= ptrW[0] * ptrA[0];
        }
        w[0] -= w[1] * ptrA[5];

        return w;
    }



    Splines::SmoothingCubicHermitSpline::SmoothingCubicHermitSpline(const std::valarray<double>& points,
                                                                    const std::valarray<double>& values,
                                                                    const std::valarray<double>& weights,
                                                                    const std::valarray<double>& segmentPoints,
                                                                    const std::valarray<double>& alpha,
                                                                    const std::valarray<double>& beta) :
        Splines::CubicHermitSpline(segmentPoints, interpolate(points, values, weights, segmentPoints, alpha, beta))
    {

    }
}