#pragma once
#include "Spline.h"

#include <array>
#include <valarray>



namespace Splines
{
    class CubicHermitSpline : public Splines::Spline
    {
    public:
        static double baseFunction1(const double& xi) { return (2.0 - (3.0 - xi * xi) * xi) / 4.0; }          //(2 - 3 * xi + xi^3) / 4
        static double baseFunction2(const double& xi) { return (1.0 - (1.0 + (1.0 - xi) * xi) * xi) / 8.0; }  //(1 - xi - xi^2 + xi^3) / 8
        static double baseFunction3(const double& xi) { return (2.0 + (3.0 - xi * xi) * xi) / 4.0; }          //(2 + 3 * xi - xi^3) / 4
        static double baseFunction4(const double& xi) { return -(1.0 + (1.0 - (1.0 + xi) * xi) * xi) / 8.0; } //(-1 - xi + xi^2 + xi^3) / 8

    private:
        std::valarray<double> m_weights;

    protected:
        CubicHermitSpline(const std::valarray<double>& points,
                          const std::valarray<double>& weights);


    public:
        std::valarray<double> getBaseFunctionsValues(const double& xi) const
        {
            return
            {
                baseFunction1(xi),
                baseFunction2(xi),
                baseFunction3(xi),
                baseFunction4(xi),
            };
        }

        std::valarray<std::valarray<double>> getBaseFunctionsValues(const std::valarray<double>& xi) const
        {
            std::size_t size = xi.size();
            std::valarray<std::valarray<double>> values(size);
            for (std::size_t i = 0; i < size; i++)
            {
                values[i] = getBaseFunctionsValues(xi[i]);
            }

            return values;
        }

        template <std::size_t n>
        std::array<std::valarray<double>, n> getBaseFunctionsValues(const std::array<double, n>& xi) const
        {
            std::array<std::valarray<double>, n> values;
            for (std::size_t i = 0; i < n; i++)
            {
                values[i] = getBaseFunctionsValues(xi[i]);
            }

            return values;
        }

        double calculateSquareError(const std::size_t& segmentIndex, const ScalarFunction& f) const override;

        const std::valarray<double>& getWeights() const { return m_weights; }

        double getValue(const std::size_t& segmentIndex, const double& xi) const override;
    };
}