#include "CubicHermitSpline.h"

#include "Gauss.h"



namespace Splines
{
    Splines::CubicHermitSpline::CubicHermitSpline(const std::valarray<double>& points,
                                                  const std::valarray<double>& weights) :
                                                  Splines::Spline(points), m_weights(weights)
    {

    }



    double Splines::CubicHermitSpline::calculateSquareError(const std::size_t& segmentIndex, const ScalarFunction& f) const
    {
        static constexpr std::size_t numberOfGaussPoints = 7;
        static const std::array<double, numberOfGaussPoints>& gaussPoints1d = Gauss::Gauss<numberOfGaussPoints, 1>::Points1d;
        static const std::array<double, numberOfGaussPoints>& gaussWeights1d = Gauss::Gauss<numberOfGaussPoints, 1>::Weights1d;
        static const std::array<std::valarray<double>, numberOfGaussPoints> phiAtGaussPoints = getBaseFunctionsValues(gaussPoints1d);



        const std::valarray<double>& points = getPoints();
        double center = (points[segmentIndex + 1] + points[segmentIndex]) / 2.0,
               length = points[segmentIndex + 1] - points[segmentIndex],
               halfLength = length / 2.0,
               norm = 0;
        for (std::size_t gaussPointIndex = 0; gaussPointIndex < numberOfGaussPoints; gaussPointIndex++)
        {
            double value = 0, x = center + halfLength * gaussPoints1d[gaussPointIndex];
            const std::valarray<double>& phi = phiAtGaussPoints[gaussPointIndex];
            value = phi[0] * m_weights[2 * segmentIndex + 0] +
                    length * phi[1] * m_weights[2 * segmentIndex + 1] +
                    phi[2] * m_weights[2 * segmentIndex + 2] +
                    length * phi[3] * m_weights[2 * segmentIndex + 3] - f(x);

            norm += gaussWeights1d[gaussPointIndex] * value * value;
        }

        return halfLength * norm;
    }



    double Splines::CubicHermitSpline::getValue(const std::size_t& segmentIndex, const double& xi) const
    {
        const std::valarray<double>& points = getPoints();
        double length = points[segmentIndex + 1] - points[segmentIndex];

        return baseFunction1(xi) * m_weights[2 * segmentIndex + 0] +
               length * baseFunction2(xi) * m_weights[2 * segmentIndex + 1] +
               baseFunction3(xi) * m_weights[2 * segmentIndex + 2] +
               length * baseFunction4(xi) * m_weights[2 * segmentIndex + 3];
    }
}