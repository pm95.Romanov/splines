#pragma once
#include "Spline.h"

#include <array>
#include <valarray>



namespace Splines
{
    class InterpolationCubicSpline : public Splines::Spline
    {
    private:
        std::valarray<double> m_weights;

        static std::valarray<double> interpolate(const std::valarray<double>& points,
                                                 const std::valarray<double>& values);

    public:
        InterpolationCubicSpline(const std::valarray<double>& points,
                                 const std::valarray<double>& values);



        double calculateSquareError(const std::size_t& segmentIndex, const ScalarFunction& f) const override;

        const std::valarray<double>& getWeights() const { return m_weights; }

        double getValue(const std::size_t& segmentIndex, const double& xi) const override;
    };
}
