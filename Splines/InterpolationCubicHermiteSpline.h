#pragma once
#include "CubicHermitSpline.h"

#include <array>
#include <valarray>



namespace Splines
{
    enum JoiningFragmentsType
    {
        APPROXE_FIRST_DERIVATIVE,
        SMOOTHNESS
    };

    class InterpolationCubicHermiteSpline : public Splines::CubicHermitSpline
    {
    private:
        static std::valarray<double> interpolate(const std::valarray<double>& points,
                                                 const std::valarray<double>& values,
                                                 const Splines::JoiningFragmentsType& joiningFragmentsType);
        static std::valarray<double> interpolateWithDerivativeApproximation(const std::valarray<double>& points,
                                                                            const std::valarray<double>& values);
        static std::valarray<double> interpolateWithSmoothness(const std::valarray<double>& points,
                                                               const std::valarray<double>& values);

    public:
        const JoiningFragmentsType JoiningFragmentsType;

        InterpolationCubicHermiteSpline(const std::valarray<double>& points,
                                        const std::valarray<double>& values,
                                        const Splines::JoiningFragmentsType& joiningFragmentsType);
    };
}
