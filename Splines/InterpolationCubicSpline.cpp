#include "InterpolationCubicSpline.h"

#include "Gauss.h"



namespace Splines
{
    std::valarray<double> Splines::InterpolationCubicSpline::interpolate(const std::valarray<double>& points,
                                                                         const std::valarray<double>& values)
    {
        std::size_t numberOfPoints = points.size(),
                    numberOfSegments = numberOfPoints - 1;
        if (numberOfPoints < 3)
        {
            throw std::invalid_argument("Points.size() < 3");
        }

        std::valarray<double> weights(4 * numberOfSegments);
        std::valarray<double> A(2 * numberOfSegments - 1);
        double currentH = points[1] - points[0],
               nextH = points[2] - points[1];

        A[0] = 2.0 * (currentH + nextH);
        A[1] = currentH;

        weights[0] = values[1];
        weights[1] = (values[1] - values[0]) / currentH;
        weights[5] = (values[2] - values[1]) / nextH;
        weights[2] = 3.0 * (weights[5] - weights[1]);

        for (std::size_t row = 1; row < numberOfSegments - 1; row++)
        {
            currentH = nextH;
            nextH = points[row + 2] - points[row + 1];
            double k = currentH / A[2 * row - 2];

            A[2 * row + 0] = 2.0 * (currentH + nextH) - k * A[2 * row - 1];
            A[2 * row + 1] = currentH;

            weights[4 * row + 0] = values[row + 1];
            weights[4 * row + 5] = (values[row + 2] - values[row + 1]) / nextH;
            weights[4 * row + 2] = 3.0 * (weights[4 * row + 5] - weights[4 * row + 1]) - k * weights[4 * row - 2];
        }

        currentH = nextH;

        weights[4 * numberOfSegments - 4] = values[numberOfSegments];
        weights[4 * numberOfSegments - 2] = 0;
        weights[4 * numberOfSegments - 1] = 0;

        for (std::make_signed_t<std::size_t> row = numberOfSegments - 2; row >= 0; row--)
        {
            weights[4 * row + 2] = (weights[4 * row + 2] - A[2 * row + 1] * weights[4 * row + 6]) / A[2 * row];

            nextH = currentH;
            currentH = points[row + 1] - points[row];

            weights[4 * row + 1] += 2.0 * currentH * weights[4 * row + 2] / 3.0;
            weights[4 * row + 3] += weights[4 * row + 2] / (3.0 * currentH);

            weights[4 * row + 5] += nextH * weights[4 * row + 2] / 3.0;
            weights[4 * row + 7] -= weights[4 * row + 2] / (3.0 * nextH);
        }

        return weights;
    }



    Splines::InterpolationCubicSpline::InterpolationCubicSpline(const std::valarray<double>& points,
                                                                const std::valarray<double>& values) :
        Splines::Spline(points), m_weights(interpolate(points, values))
    {

    }



    double Splines::InterpolationCubicSpline::calculateSquareError(const std::size_t& segmentIndex, const ScalarFunction& f) const
    {
        static constexpr std::size_t numberOfGaussPoints = 5;
        static const std::array<double, numberOfGaussPoints>& gaussPoints1d = Gauss::Gauss<5, 1>::Points1d;
        static const std::array<double, numberOfGaussPoints>& gaussWeights1d = Gauss::Gauss<5, 1>::Weights1d;

        const std::valarray<double>& points = getPoints();
        double center = (points[segmentIndex + 1] + points[segmentIndex]) / 2.0,
               length = points[segmentIndex + 1] - points[segmentIndex],
               halfLength = length / 2.0,
               norm = 0;

        for (std::size_t gaussPointIndex = 0; gaussPointIndex < numberOfGaussPoints; gaussPointIndex++)
        {
            double value = 0, buf, x = center + halfLength * gaussPoints1d[gaussPointIndex];

            value = -f(x);
            x -= points[segmentIndex + 1];
            value += m_weights[4 * segmentIndex];
            buf = x;
            value += m_weights[4 * segmentIndex + 1] * buf;
            buf *= x;
            value += m_weights[4 * segmentIndex + 2] * buf;
            buf *= x;
            value += m_weights[4 * segmentIndex + 3] * buf;
            norm += gaussWeights1d[gaussPointIndex] * value * value;
        }

        return halfLength * norm;
    }



    double Splines::InterpolationCubicSpline::getValue(const std::size_t& segmentIndex, const double& xi) const
    {
        const std::valarray<double>& points = getPoints();
        double center = (points[segmentIndex + 1] + points[segmentIndex]) / 2.0,
               length = points[segmentIndex + 1] - points[segmentIndex],
               halfLength = length / 2.0,
               norm = 0;

        double value = 0, buf, x = center + halfLength * xi - points[segmentIndex + 1];
        value = m_weights[4 * segmentIndex];
        buf = x;
        value += m_weights[4 * segmentIndex + 1] * buf;
        buf *= x;
        value += m_weights[4 * segmentIndex + 2] * buf;
        buf *= x;
        value += m_weights[4 * segmentIndex + 3] * buf;

        return value;
    }
}