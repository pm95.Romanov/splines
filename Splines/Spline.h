#pragma once
#include <algorithm>
#include <functional>
#include <stdexcept>
#include <valarray>



namespace Splines
{
    using ScalarFunction = std::function<double(const double& x)>;

    int compare0(const double& x, const double& eps);
    int compare(const double& x, const double& y, const double& eps);



    class NotImplementedException : public std::logic_error
    {
    public:
        NotImplementedException() : std::logic_error("Function not yet implemented") { };
    };



    class Spline
    {
    private:
        std::valarray<double> m_points;

    public:
        Spline(const std::valarray<double>& points) : m_points(points) {}
        Spline(std::valarray<double>&& points) : m_points(points) {}

        const std::valarray<double>& getPoints() const { return m_points; }

        virtual bool findSegment(const double& x, std::size_t& beginIndex, std::size_t& endIndex) const;

        virtual double calculateSquareError(const std::size_t& segmentIndex, const ScalarFunction& f) const = 0;

        virtual double getValue(const double& x) const;
        virtual double getValue(const std::size_t& segmentIndex, const double& xi) const = 0;
    };
}
