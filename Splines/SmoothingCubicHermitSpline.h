#pragma once
#include "CubicHermitSpline.h"



namespace Splines
{
    class SmoothingCubicHermitSpline : public Splines::CubicHermitSpline
    {
    private:
        static std::valarray<double> interpolate(const std::valarray<double>& points,
                                                 const std::valarray<double>& values,
                                                 const std::valarray<double>& weights,
                                                 const std::valarray<double>& segmentPoints,
                                                 const std::valarray<double>& alpha,
                                                 const std::valarray<double>& beta);

    public:
        SmoothingCubicHermitSpline(const std::valarray<double>& points,
                                   const std::valarray<double>& values,
                                   const std::valarray<double>& weights,
                                   const std::valarray<double>& segmentPoints,
                                   const std::valarray<double>& alpha,
                                   const std::valarray<double>& beta);
    };
}
