#include "InterpolationQuadraticSpline.h"

#include "Gauss.h"



namespace Splines
{
    std::valarray<double> Splines::InterpolationQuadraticSpline::interpolate(const std::valarray<double>& points,
                                                                             const std::valarray<double>& values)
    {
        std::size_t numberOfPoints = points.size(),
                    numberOfSegments = numberOfPoints - 1;
        if (numberOfPoints < 3)
        {
            throw std::invalid_argument("Points.size() < 3");
        }

        std::valarray<double> weights(4 * numberOfSegments);
        double prevH = points[1] - points[0],
               currentH = points[2] - points[1],
               sumH = points[2] - points[0];

        weights[0] = values[1];
        weights[1] = -(prevH + sumH) / (prevH * sumH) * values[0] +
                      sumH / (prevH * currentH) * values[1] -
                      prevH / (sumH * currentH) * values[2];
        weights[2] = ((values[1] - values[0]) / prevH - weights[1]) / prevH;
        weights[1] += 2 * weights[2] * prevH;
        
        weights[3] = values[2];
        weights[5] = ((values[2] - values[1]) / currentH - weights[1]) / currentH;
        weights[4] = weights[1] + 2 * weights[5] * currentH;

        for (std::size_t i = 2; i < numberOfSegments; i++)
        {
            weights[3 * i + 0] = values[i + 1];
            weights[3 * i + 2] = ((values[i + 1] - values[i]) / currentH - weights[3 * i - 2]) / currentH;
            weights[3 * i + 1] = weights[3 * i - 2] + 2 * weights[3 * i + 2] * currentH;
        }

        return weights;
    }



    Splines::InterpolationQuadraticSpline::InterpolationQuadraticSpline(const std::valarray<double>& points,
                                                                        const std::valarray<double>& values) :
        Splines::Spline(points), m_weights(interpolate(points, values))
    {

    }



    double Splines::InterpolationQuadraticSpline::calculateSquareError(const std::size_t& segmentIndex, const ScalarFunction& f) const
    {
        static constexpr std::size_t numberOfGaussPoints = 5;
        static const std::array<double, numberOfGaussPoints>& gaussPoints1d = Gauss::Gauss<5, 1>::Points1d;
        static const std::array<double, numberOfGaussPoints>& gaussWeights1d = Gauss::Gauss<5, 1>::Weights1d;

        const std::valarray<double>& points = getPoints();
        double center = (points[segmentIndex + 1] + points[segmentIndex]) / 2.0,
               length = points[segmentIndex + 1] - points[segmentIndex],
               halfLength = length / 2.0,
               norm = 0;

        for (std::size_t gaussPointIndex = 0; gaussPointIndex < numberOfGaussPoints; gaussPointIndex++)
        {
            double value = 0, buf, x = center + halfLength * gaussPoints1d[gaussPointIndex];

            value = -f(x);
            x -= points[segmentIndex + 1];
            value += m_weights[3 * segmentIndex];
            buf = x;
            value += m_weights[3 * segmentIndex + 1] * buf;
            buf *= x;
            value += m_weights[3 * segmentIndex + 2] * buf;
            norm += gaussWeights1d[gaussPointIndex] * value * value;
        }

        return halfLength * norm;
    }



    double Splines::InterpolationQuadraticSpline::getValue(const std::size_t& segmentIndex, const double& xi) const
    {
        const std::valarray<double>& points = getPoints();
        double center = (points[segmentIndex + 1] + points[segmentIndex]) / 2.0,
               length = points[segmentIndex + 1] - points[segmentIndex],
               halfLength = length / 2.0,
               norm = 0;

        double value = 0, buf, x = center + halfLength * xi - points[segmentIndex + 1];
        value = m_weights[3 * segmentIndex];
        buf = x;
        value += m_weights[3 * segmentIndex + 1] * buf;
        buf *= x;
        value += m_weights[3 * segmentIndex + 2] * buf;

        return value;
    }
}
